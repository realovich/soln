$(document).ready(function(){


$("body").on("click",".linkto", function (event) {
  event.preventDefault();
  var id = $(this).attr('href'),
  top = $(id).offset().top;
  $('body,html').animate({scrollTop: top}, 750);
  $('.top-menu, .main-btn').removeClass('open');
});


$('.main-btn').click(function() {
  $(this).toggleClass('open');
  $('.top-menu').toggleClass('open');
});

$('.header').stick_in_parent({
  parent: '.wrapper'
});

});


$('.top-slider').slick({
  arrows: true,
  dots: false,
  infinite: false,
  speed: 1250,
  fade: true,
  slidesToShow: 1
});
